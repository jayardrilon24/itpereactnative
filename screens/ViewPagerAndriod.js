import React, { Component } from 'react';
import { View, Text, viewPager, StyleSheet, AppRegistry } from "react-native";

class ViewScreenpage extends Component {
    render() {
  return (
    <ViewPagerAndroid
      style={styles.viewPager}
      initialPage={0}>
      <View style={styles.pageStyle} key="1">
        <Text>First page</Text>
      </View>
      <View style={styles.pageStyle} key="2">
        <Text>Second page</Text>
      </View>
    </ViewPagerAndroid>
  );
}

}
